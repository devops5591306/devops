Principes de base du Réseau: Networking 101 Comprendre les concepts de région et de zone Pour garantir un déploiement efficace de nos ressources, il est crucial de préétablir les régions et les zones géographiques. Cette pratique permet d'optimiser à la fois la latence et les coûts.

gcloud config set compute/zone "Zone"
export ZONE=$(gcloud config get compute/zone)

gcloud config set compute/region "Region"
export REGION=$(gcloud config get compute/region)

Création d'un réseau VPC personnalisé La première étape consiste à mettre en place un réseau VPC personnalisé. Ce réseau jouera le rôle fondamental de notre infrastructure cloud en facilitant la communication entre les différentes ressources à déployer.

gcloud compute networks create taw-custom-network --subnet-mode custom

Ajout de sous-réseaux Une fois le réseau VPC établi, nous créons des sous-réseaux dans différentes régions. Cette segmentation permet de diviser notre réseau en blocs plus gérables, facilitant ainsi la mise en œuvre des politiques de sécurité.

gcloud compute networks subnets create subnet-Region \
   --network taw-custom-network \
   --region Region \
   --range 10.0.0.0/16
gcloud compute networks subnets create subnet-Region \
   --network taw-custom-network \
   --region Region \
   --range 10.1.0.0/16
gcloud compute networks subnets create subnet-Region \
   --network taw-custom-network \
   --region Region \
   --range 10.2.0.0/16

Configuration des règles de pare-feu Pour sécuriser notre réseau et contrôler l'accès aux instances VM, nous définissons des règles de pare-feu. Dans cet exemple, nous créons une règle autorisant le trafic HTTP entrant.

gcloud compute firewall-rules create nw101-allow-http \
   --allow tcp:80 \
   --network taw-custom-network \
   --source-ranges 0.0.0.0/0 \
   --target-tags http

Ensuite nous allons afficher la liste de mes réseaux

gcloud compute networks subnets list \
   --network taw-custom-network

Déploiement d'instances VM Enfin, nous déployons des instances VM dans notre réseau, en les assignant aux sous-réseaux appropriés. Ces instances serviront d'hôtes pour nos applications et services.

gcloud compute firewall-rules create nw101-allow-http \
--allow tcp:80 --network taw-custom-network --source-ranges 0.0.0.0/0 \
--target-tags http


Ajouter des règles de pare-feu supplémentaires
Pour une couverture de sécurité plus complète, il est important d'ajouter des règles de pare-feu supplémentaires qui autorisent le trafic ICMP (pour les pings), SSH (pour les connexions sécurisées à nos instances), et potentiellement RDP (pour l'accès à distance sous Windows).

gcloud compute firewall-rules create nw101-allow-http \
--allow tcp:80 --network taw-custom-network --source-ranges 0.0.0.0/0 \
--target-tags http

gcloud compute firewall-rules create "nw101-allow-icmp" --allow icmp --network "taw-custom-network" --target-tags rules
gcloud compute firewall-rules create "nw101-allow-internal" --allow tcp:0-65535,udp:0-65535,icmp --network "taw-custom-network" --source-ranges "10.0.0.0/16","10.2.0.0/16","10.1.0.0/16"
gcloud compute firewall-rules create "nw101-allow-ssh" --allow tcp:22 --network "taw-custom-network" --target-tags "ssh"
gcloud compute firewall-rules create "nw101-allow-rdp" --allow tcp:3389 --network "taw-custom-network"


Créer des instances de machine virtuelle supplémentaires
Afin de tester la connectivité et les performances entre différentes zones et zones, c'est une bonne idée de déployer des instances de machine virtuelle supplémentaires dans les sous-réseaux que nous avons créés.

gcloud compute instances create us-test-01 \
--subnet subnet-Region \
--zone ZONE \
--machine-type e2-standard-2 \
--tags ssh,http,rules
gcloud compute instances create us-test-02 \
--subnet subnet-REGION \
--zone ZONE \
--machine-type e2-standard-2 \
--tags ssh,http,rules
gcloud compute instances create us-test-03 \
--subnet subnet-REGION \
--zone ZONE \
--machine-type e2-standard-2 \
--tags ssh,http,rules


Évaluer la connectivité et la performance.
la suite de la configuration du réseau, il est important de tester la connectivité réseau et d'évaluer la performance. Pour tester la connectivité, on peut utiliser des tests de ping, des outils comme iperf et des tests de bande passante.


- # Sur l'instance sourceping -c 3 <adresse IP externe de l'instance cible># Installation d'iperf pour les tests de performancesudo apt-get update && sudo apt-get install -y iperf# Sur l'instance servant de serveur (écoute sur le port par défaut 5001)iperf -s# Sur l'instance cliente, pour initier le testiperf -c <adresse IP interne de l'instance serveur> -t 30



TerraForm


La commande gcloud auth list affiche les comptes Google Cloud qui sont authentifiés sur l'appareil local, permettant ainsi de visualiser les comptes connectés et actifs pour l'utilisation des fonctionnalités de gcloud.

gcloud auth list

En bash, exécuter la commande touch instance.tf crée un fichier vide nommé "instance.tf". Ensuite, ouvrez l'éditeur de texte de Google et insérez le code suivant dans le fichier instance.tf.

touch instance.tf

Ensuite aller dans doc de texte de google et ajouter le code ci-dessous dans instance.tf

resource "google_compute_instance" "terraform" {
  project      = "mon identifiant"
  name         = "terraform"
  machine_type = "e2-medium"
  zone         = "us-east1-b"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }
}

La commande terraform init initialise un nouvel espace de travail Terraform. Elle prépare le répertoire en configurant les plugins et les modules requis pour gérer l'infrastructure définie dans les fichiers de configuration Terraform.

terraform init

Suite au Terraform Init, voici le résultat donné:
resource "google_compute_instance" "terraform" {
  project      = "qwiklabs-gcp-01-aefd70bc4ef6"
  name         = "terraform"
  machine_type = "e2-medium"
  zone         = "us-east1-b"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }
}
terraform init
Initializing the backend...

Initializing provider plugins...
- Finding latest version of hashicorp/google...
- Installing hashicorp/google v5.15.0...
- Installed hashicorp/google v5.15.0 (signed by HashiCorp)

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.


La commande terraform plan génère un plan d'action pour Terraform. Elle examine les fichiers de configuration Terraform dans le répertoire de travail et présente un récapitulatif des opérations que Terraform effectuera pour mettre en place l'infrastructure spécifiée. Cela offre une vision préliminaire des modifications envisagées avant de les appliquer effectivement.
 
 Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # google_compute_instance.terraform will be created
  + resource "google_compute_instance" "terraform" {
      + can_ip_forward       = false
      + cpu_platform         = (known after apply)
      + current_status       = (known after apply)
      + deletion_protection  = false
      + effective_labels     = (known after apply)
      + guest_accelerator    = (known after apply)
      + id                   = (known after apply)
      + instance_id          = (known after apply)
      + label_fingerprint    = (known after apply)
      + machine_type         = "e2-medium"
      + metadata_fingerprint = (known after apply)
      + min_cpu_platform     = (known after apply)
      + name                 = "terraform"
      + project              = "mon identifiant"
      + self_link            = (known after apply)
      + tags_fingerprint     = (known after apply)
      + terraform_labels     = (known after apply)
      + zone                 = "us-east1-b"

      + boot_disk {
          + auto_delete                = true
          + device_name                = (known after apply)
          + disk_encryption_key_sha256 = (known after apply)
          + kms_key_self_link          = (known after apply)
          + mode                       = "READ_WRITE"
          + source                     = (known after apply)

          + initialize_params {
              + image                  = "debian-cloud/debian-11"
              + labels                 = (known after apply)
              + provisioned_iops       = (known after apply)
              + provisioned_throughput = (known after apply)
              + size                   = (known after apply)
              + type                   = (known after apply)
            }
        }
              + network_interface {
          + internal_ipv6_prefix_length = (known after apply)
          + ipv6_access_type            = (known after apply)
          + ipv6_address                = (known after apply)
          + name                        = (known after apply)
          + network                     = "default"
          + network_ip                  = (known after apply)
          + stack_type                  = (known after apply)
          + subnetwork                  = (known after apply)
          + subnetwork_project          = (known after apply)

          + access_config {
              + nat_ip       = (known after apply)
              + network_tier = (known after apply)
            }
        }
    }

Plan: 1 to add, 0 to change, 0 to destroy.



La commande terraform apply permet de mettre en œuvre les modifications décrites dans les fichiers de configuration Terraform. Elle crée, met à jour ou supprime les ressources conformément aux modifications apportées au code Terraform depuis la dernière exécution. Avant d'appliquer les changements, Terraform présente un aperçu des actions qu'il va entreprendre et demande confirmation avant de procéder.

terraform apply

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # google_compute_instance.terraform will be created
  + resource "google_compute_instance" "terraform" {
      + can_ip_forward       = false
      + cpu_platform         = (known after apply)
      + current_status       = (known after apply)
      + deletion_protection  = false
      + effective_labels     = (known after apply)
      + guest_accelerator    = (known after apply)
      + id                   = (known after apply)
      + instance_id          = (known after apply)
      + label_fingerprint    = (known after apply)
      + machine_type         = "e2-medium"
      + metadata_fingerprint = (known after apply)
      + min_cpu_platform     = (known after apply)
      + name                 = "terraform"
      + project              = "Mon identifiant"
      + self_link            = (known after apply)
      + tags_fingerprint     = (known after apply)
      + terraform_labels     = (known after apply)
      + zone                 = "us-east1-b"

      + boot_disk {
          + auto_delete                = true
          + device_name                = (known after apply)
          + disk_encryption_key_sha256 = (known after apply)
          + kms_key_self_link          = (known after apply)
          + mode                       = "READ_WRITE"
          + source                     = (known after apply)

          + initialize_params {
              + image                  = "debian-cloud/debian-11"
              + labels                 = (known after apply)
              + provisioned_iops       = (known after apply)
              + provisioned_throughput = (known after apply)
              + size                   = (known after apply)
              + type                   = (known after apply)
            }
        }
      + network_interface {
          + internal_ipv6_prefix_length = (known after apply)
          + ipv6_access_type            = (known after apply)
          + ipv6_address                = (known after apply)
          + name                        = (known after apply)
          + network                     = "default"
          + network_ip                  = (known after apply)
          + stack_type                  = (known after apply)
          + subnetwork                  = (known after apply)
          + subnetwork_project          = (known after apply)

          + access_config {
              + nat_ip       = (known after apply)
              + network_tier = (known after apply)
            }
        }
    }

Plan: 1 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

google_compute_instance.terraform: Creating...
google_compute_instance.terraform: Still creating... [10s elapsed]
google_compute_instance.terraform: Creation complete after 18s [id=projects/mon identifiant/zones/us-east1-b/instances/terraform]

Pour terminer nous ferons un terraform show pour voir l'état actuel

# google_compute_instance.terraform:
resource "google_compute_instance" "terraform" {
    can_ip_forward       = false
    cpu_platform         = "Intel Broadwell"
    current_status       = "RUNNING"
    deletion_protection  = false
    effective_labels     = {}
    enable_display       = false
    guest_accelerator    = []
    id                   = "projects/mon identifiant/zones/us-east1-b/instances/terraform"
    instance_id          = "3631703849039533754"
    label_fingerprint    = "42WmSpB8rSM="
    machine_type         = "e2-medium"
    metadata_fingerprint = "WRRcWNyHo0s="
    name                 = "terraform"
    project              = "qwiklabs-gcp-01-aefd70bc4ef6"
    self_link            = "https://www.googleapis.com/compute/v1/projects/mon identifiant/zones/us-east1-b/instances/terraform"
    tags_fingerprint     = "42WmSpB8rSM="
    terraform_labels     = {}
    zone                 = "us-east1-b"

    boot_disk {
        auto_delete = true
        device_name = "persistent-disk-0"
        mode        = "READ_WRITE"
        source      = "https://www.googleapis.com/compute/v1/projects/mon identifiant/zones/us-east1-b/disks/terraform"

        initialize_params {
            enable_confidential_compute = false
            image                       = "https://www.googleapis.com/compute/v1/projects/debian-cloud/global/images/debian-11-bullseye-v20240110"
            labels                      = {}
            provisioned_iops            = 0
            provisioned_throughput      = 0
            size                        = 10
            type                        = "pd-standard"
        }
    }

    network_interface {
        internal_ipv6_prefix_length = 0
        name                        = "nic0"
        network                     = "https://www.googleapis.com/compute/v1/projects/mon identifiant/global/networks/default"
        network_ip                  = "10.142.0.2"
        queue_count                 = 0
        stack_type                  = "IPV4_ONLY"
        subnetwork                  = "https://www.googleapis.com/compute/v1/projects/mon identifiant/regions/us-east1/subnetworks/default"
        subnetwork_project          = "qwiklabs-gcp-01-aefd70bc4ef6"

        access_config {
            nat_ip       = "34.138.108.138"
            network_tier = "PREMIUM"
        }
    }

    scheduling {
        automatic_restart   = true
        min_node_cpus       = 0
        on_host_maintenance = "MIGRATE"
        preemptible         = false
        provisioning_model  = "STANDARD"
    }

    shielded_instance_config {
        enable_integrity_monitoring = true
        enable_secure_boot          = false
        enable_vtpm                 = true
    }
}


